<?php

    error_reporting(E_ALL);

    $consumerKey = 'PIOyTJvMmMQisOsjNHVu';
    $consumerSecret = 'ickLdRzbvwqfjVnaWJoDFzTiJdSUgLyz';

    require '../vendor/autoload.php';

    use OAuth\OAuth1\Service\BitBucket;
    use OAuth\Common\Storage\Session;
    use OAuth\Common\Consumer\Credentials;

    ini_set('date.timezone', 'Europe/Amsterdam');

    $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
    $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
    $currentUri->setQuery('');

    $serviceFactory = new \OAuth\ServiceFactory();

// We need to use a persistent storage to save the token, because oauth1 requires the token secret received before'
// the redirect (request token request) in the access token request.
    $storage = new Session();
// Setup the credentials for the requests
    $credentials = new Credentials(
        $consumerKey, $consumerSecret, $currentUri->getAbsoluteUri()
    );
// Instantiate the BitBucket service using the credentials, http client and storage mechanism for the token
    /** @var $bbService BitBucket */
    $bbService = $serviceFactory->createService('Discogs', $credentials, $storage);

    if (isset($_GET['oauth_token'])) {
        $token = $storage->retrieveAccessToken('Discogs');

        $bbService->requestAccessToken(
            $_GET['oauth_token'], $_GET['oauth_verifier'], $token->getRequestTokenSecret()
        );

        header("Location: /");
        exit();
    }

    $isAuthorized = true;
    try {
        $token = $storage->retrieveAccessToken('Discogs');
    } catch (\OAuth\Common\Storage\Exception\TokenNotFoundException $e) {
        $isAuthorized = false;
    }

    if (!$isAuthorized) {
        $token = $bbService->requestRequestToken();
        $url = $bbService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
        header('Location: ' . $url);
    }

    $client = Discogs\ClientFactory::factory();
    $client->getHttpClient()->getEmitter()->attach(new Discogs\Subscriber\ThrottleSubscriber());
    $oauth = new GuzzleHttp\Subscriber\Oauth\Oauth1([
        'consumer_key' => $consumerKey, // from Discogs developer page
        'consumer_secret' => $consumerSecret, // from Discogs developer page
        'token' => $token->getRequestToken(), // get this using a OAuth library
        'token_secret' => $token->getRequestTokenSecret() // get this using a OAuth library
    ]);
    $client->getHttpClient()->getEmitter()->attach($oauth);


    /* Data retrieve */

    function getHeader($path)
    {
        $file = fopen($path, 'r') or die($php_errormsg);
        $data = fgetcsv($file, 0, ',');
        fclose($file);
        return $data;
    }

    function getData($path)
    {
        $file = fopen($path, 'r') or die($php_errormsg);
        $data = array();
        $header = getHeader($path);
        //var_dump($file);
        //var_dump($header);
        if (!empty($file)) {
            while (($row = fgetcsv($file, 0, ',')) !== FALSE) {
                //var_dump(array_combine($header, $row));
                //var_dump($header);
                //var_dump($row);
                $arr = array_combine($header, $row);
                foreach ($header as $i => $col) {
                    $arr[$col] = $row[$i];
                }
                $data[] = $arr;
            }
            return $data;
        }
        fclose($file);
        //var_dump($data);
    }

//var_dump($newData);
//var_dump(getData());
    function writeIds($client, $path, $prefix)
    {
        if (empty($prefix)) {
            die('Prefix empty');
        }
        $today = date("njY");
        $new = fopen('../files/update-' . $prefix . '-' . $today . '.csv', 'w') or die($php_errormsg);
        $data = getData($path);
        $header = getHeader($path);
        array_push($header, 'disc_release_id');
        //var_dump($header);
        //var_dump($combineHeader);

        fputcsv($new, array_values($header), ',');
        $i = 0;
        foreach ($data as $d) {
            if ($i >= 1) {
//            $barcode = $d['Barcode'];
                $title = $d['Title'];
                $catNumber = $d['Catalogue Item'];
//            if ($barcode) {
                if ($title && $catNumber) {
                    $response = $client->search([
                        'q' => $title . ' ' . $catNumber
//                    'q' => $barcode
                    ]);
                    $ids = [];

                    foreach ($response['results'] as $result) {
                        $ids[] = $result['id'];
                    }
                    $d['disc_release_id'] = implode(',', $ids);
                    //var_dump($ids);
                    fputcsv($new, array_values($d), ',');
                    sleep(2);
                    //flush();
                    //ob_flush();
                    //var_dump($ids);
                    //var_dump($d);
                }
            }
            //return $ids;
            $i++;
        }

        fclose($new);
    }

    function download_image($image_url, $image_file)
    {
        $fp = fopen($image_file, 'w+') or die($php_errormsg);              // open file handle
        $ch = curl_init($image_url);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
        curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
        curl_exec($ch);
        curl_close($ch);                              // closing curl handle
        fclose($fp);                                  // closing file handle
    }

    function writeFullData($client, $path)
    {
        //$path = $file;
        $header = getHeader($path);
        $data = getData($path);
        $headerAdd = [
//        'music_release_date',
//        'record_label',
//        'catalog_number',
//        'format_qty',
//        'category_ids',
//        'name',
//        'music_artist',
//        'music_release_country',
//        'description',
//        'music_release_tracklist',
//        'weight',
//        'image',
//        'small_image',
//        'thumbnail',
//        'image_label',
//        'small_image_label',
//        'thumbnail_label',
//        'media_gallery',
//        'music_format',
//        'music_style',
//        'music_video',
            'discogs_release_want',
            'discogs_release_have'
        ];
        $combineHeader = array_merge($header, $headerAdd);

        $file = fopen($path, 'r+') or die($php_errormsg);
        //$data = fgetcsv($file, 0, ';');
        fputcsv($file, array_values($combineHeader), ',');
        $i = 0;
        //echo 'aaaa';
        //var_dump($data);
        //var_dump($row);
        foreach ($data as $d) {
            if ($i >= 1) {
                //var_dump($d);
                $releaseId = $d['disc_release_id'] ? trim($d['disc_release_id']) : '';
                if ($releaseId) {
                    var_dump($client);
                    $response = $client->getRelease([
                        'id' => $releaseId
                    ]);
                    var_dump($response);
////                if ($i == 5)
////                    break;
//// Release date
//                $releaseDate = $response['released'] ? $response['released'] : '';
//                if ($releaseDate) {
//                    $m = sscanf($releaseDate, '%d-%d-%d');
//                    $time = mktime(0, 0, 0, $m[1] ?: 1, $m[2] ?: 1, $m[0]);
//                    $date = gmdate('Y-m-d', $time);
//                    $d['music_release_date'] = $date;
//                } else {
//                    $d['music_release_date'] = $releaseDate;
//                }
//                //var_dump($releaseDate);
//// Label
//                $labelName = '';
//                $catalogNumber = '';
//                $labels = $response['labels'] ? $response['labels'] : '';
//                if ($labels) {
//                    $labelNames = [];
//                    $catalogNumbers = [];
//                    foreach ($labels as $label) {
//                        $labelNames[] = $label['name'];
//                        $catalogNumbers[] = $label['catno'];
//                    }
//                    $labelName = implode(', ', array_unique($labelNames));
//                    $catalogNumber = implode(', ', array_unique($catalogNumbers));
//                }
//                //var_dump($labels);
//                $d['record_label'] = $labelName;
//                $d['catalog_number'] = $catalogNumber;
//                //var_dump($labelName);
//                //var_dump($catalogNumber);
//// Format qty
//                $formatQty = $response['format_quantity'] ? $response['format_quantity'] : '';
//                //var_dump($formatQty);
//                $d['format_qty'] = $formatQty;
//
//// Category (Genres)
//                $genres = $response['genres'] ? $response['genres'] : '';
//                if ($genres) {
//                    $genreList = [];
//                    foreach ($genres as $genre) {
//                        switch ($genre) {
//                            case 'Blues':
//                                $genreList[] = 11;
//                                break;
//                            case 'Brass & Military':
//                                $genreList[] = 241;
//                                break;
//                            case 'Children\'s':
//                                $genreList[] = 239;
//                                break;
//                            case 'Classical':
//                                $genreList[] = 42;
//                                break;
//                            case 'Electronic':
//                                $genreList[] = 20;
//                                break;
//                            case 'Folk, World, & Country':
//                                $genreList[] = 29;
//                                break;
//                            case 'Funk / Soul':
//                                $genreList[] = 21;
//                                break;
//                            case 'Jazz':
//                                $genreList[] = 10;
//                                break;
//                            case 'Hip Hop':
//                                $genreList[] = 41;
//                                break;
//                            case 'Latin':
//                                $genreList[] = 242;
//                                break;
//                            case 'Non-Music':
//                                $genreList[] = 240;
//                                break;
//                            case 'Pop':
//                                $genreList[] = 98;
//                                break;
//                            case 'Reggae':
//                                $genreList[] = 38;
//                                break;
//                            case 'Rock':
//                                $genreList[] = 5;
//                                break;
//                            case 'Stage & Screen':
//                                $genreList[] = 238;
//                                break;
//                            default:
//                                $genreList[] = $genre;
//                        }
//                    }
//                    $d['category_ids'] = implode(',', $genreList);
//                } else {
//                    //var_dump($genres);
//                    $d['category_ids'] = $genres;
//                }
//
//// Title
//                $title = $response['title'] ? $response['title'] : '';
//                //var_dump($title);
//                $d['name'] = $title;
//
//// Artist
//                $artistName = '';
//                $artists = $response['artists'] ? $response['artists'] : '';
//                if ($artists) {
//                    $artistNames = [];
//                    foreach ($artists as $artist) {
//                        $artistNames[] = $artist['name'];
//                    }
//                    $artistName = implode(', ', $artistNames);
//                }
//                //var_dump($artists);
//                //var_dump($artistName);
//                $d['music_artist'] = $artistName;
//
//// Country
//                $country = $response['country'] ? $response['country'] : '';
//                //var_dump($country);
//                $d['music_release_country'] = $country;
//
//// Notes
//                $notes = $response['notes'] ? $response['notes'] : '';
//                //var_dump($notes);
//                $d['description'] = $notes;
//
//// Tracklist
//                $tracklist = $response['tracklist'] ? $response['tracklist'] : '';
//                $tracklistHtml = '';
//                if ($tracklist) {
//                    $tracklistHtml = '<table class="playlist" itemscope="" itemtype="http://schema.org/MusicGroup"><tbody>';
//                    foreach ($tracklist as $track) {
//                        $tracklistHtml .= '<tr class="tracklist_track track" itemprop="track" itemscope="" itemtype="http://schema.org/MusicRecording">';
//                        $tracklistHtml .= '<td class="tracklist_track_pos">' . $track['position'] . '</td>';
//                        $tracklistHtml .= '<td class="track tracklist_track_title "><span class="tracklist_track_title" itemprop="name">' . $track['title'] . '</span></td>';
//                        $tracklistHtml .= '<td width="25" class="tracklist_track_duration"><meta itemprop="duration"><span>' . $track['duration'] . '</span></td>';
//                        $tracklistHtml .= '</tr>';
//                    }
//                    $tracklistHtml .= '</tbody></table>';
//                }
//                //var_dump($tracklist);
//                //var_dump($tracklistHtml);
//                $d['music_release_tracklist'] = $tracklistHtml;
//
//// Weight
//                $weight = $response['estimated_weight'] ? $response['estimated_weight'] / 1000 : '';
//                //var_dump($weight);
//                $d['weight'] = $weight;
//
//// Images
//                $productImage = '';
//                $coverImage = '';
//                $images = $response['images'] ? $response['images'] : '';
//                if ($images) {
//                    $imageList = [];
//                    $i = 1;
//                    foreach ($images as $image) {
//                        if ($image['height'] >= 450 && $image['width'] >= 450) {
//                            if ($i == 1) {
//                                $coverImage = '/images/' . basename($image['uri']);
//                            } else {
//                                $imageList[] = '+/images/' . basename($image['uri']) . '::' . $title . ' - ' . $artistName;
//                            }
//                            //var_dump(basename($image['uri']));
//                            download_image($image['uri'], '../files/images/' . basename($image['uri']));
//                        }
//                        $i++;
//                    }
//                    if (!empty($imageList)) {
//                        $productImage = implode(';', $imageList);
//                    }
//                }
//                //var_dump($images);
//                //var_dump($coverImage);
//                //var_dump($productImage);
//                $d['image'] = $coverImage;
//                $d['small_image'] = $coverImage;
//                $d['thumbnail'] = $coverImage;
//                $d['image_label'] = '';
//                $d['small_image_label'] = '';
//                $d['thumbnail_label'] = '';
//                if ($coverImage) {
//                    $d['image_label'] = $title . ' - ' . $artistName;
//                    $d['small_image_label'] = $title . ' - ' . $artistName;
//                    $d['thumbnail_label'] = $title . ' - ' . $artistName;
//                }
//                $d['media_gallery'] = $productImage;
//
//// Formats
//                $formats = $response['formats'] ? $response['formats'] : '';
//                $formatNames = '';
//                if ($formats) {
//                    $formatList = [];
//                    foreach ($formats as $format) {
//                        array_push($formatList, $format['name']);
//                        if (!empty($format['descriptions'])) {
//                            foreach ($format['descriptions'] as $desc) {
//                                $formatList[] = $desc;
//                            }
//                        }
//                    }
//                    $formatNames = implode(',', $formatList);
//                }
//                //var_dump($formats);
//                //var_dump($formatNames);
//                $d['music_format'] = $formatNames;
//
//// Styles
//                $styles = $response['styles'] ? $response['styles'] : '';
//                $stylesList = '';
//                if ($styles) {
//                    $stylesList = implode(',', $styles);
//                }
//                //var_dump($stylesList);
//                $d['music_style'] = $stylesList;
//
//// Video
//                $videos = $response['videos'] ? $response['videos'] : '';
//                $videosList = '';
//                if ($videos) {
//                    foreach ($videos as $video) {
//                        $videosList .= '<div class="video-item">';
//                        $videosList .= '<h2>' . $video['title'] . '</h2>';
//                        $videosList .= '<iframe width="560" height="315" src="' . $video['uri'] . '" frameborder="0" allowfullscreen></iframe>';
//                        $videosList .= '</div>';
//                    }
//                }
//                //var_dump($videosList);
//                $d['music_video'] = $videosList;

// Discogs release want count
                    $want = $response['community']['want'] ? $response['community']['want'] : '';
                    $d['discogs_release_want'] = $want;

// Discogs release have count
                    $have = $response['community']['have'] ? $response['community']['have'] : '';
                    $d['discogs_release_have'] = $have;

                    fputcsv($file, array_values($d), ',');
                        sleep(3);
                    //var_dump($d);
                }
            }
            $i++;
        }

        fclose($file);
    }

//writeIds($client, '../files/sony-update-061115.csv', 'sony');
    writeFullData($client, '../files/get_want.csv');

    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//flush();
//ob_flush();
    echo "Process Time: {$time}";
    //$response = $client->getRelease([
    //    'id' => 6532983
    //        ]);
    //echo '<pre>';
    //print_r($response);

    //var_dump($coverImage);
    //var_dump($productImage);


    //print_r($response);
    //print_r($ids);
